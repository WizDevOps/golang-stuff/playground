package main

import (
	"fmt"
	"time"
)

func main() {
	Publish("A goroutine starts a new thread.", 5*time.Second)
	fmt.Println("Hope the news will publish before I leave")

	time.Sleep(10 * time.Second)

	fmt.Println("Ten seconds later: I'm leaving now")
}

func Publish(text string, delay time.Duration) {
	go func() {
		time.Sleep(delay)
		fmt.Println("BREAKING NEWS: ", text)
	}()
}