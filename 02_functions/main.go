package main

import "fmt"

func adder(x int) int {
	return x + 1
}

func main() {
	foobar := "hello, I'm working without 'var'"
	fmt.Println(adder(5), foobar)
}
