package main

import "fmt"

type Employee struct {
	name, currency string
	salary         int
}

func (e *Employee) displaySalary() {
	fmt.Printf("Salary of %s is %s %d", e.name, e.currency, e.salary)
}

func main() {
	emp1 := Employee{
		name:     "Adolf Heimann",
		salary:   52000,
		currency: "€",
	}
	emp1.displaySalary()
}
