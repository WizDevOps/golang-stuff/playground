package main

import "fmt"

func Hello(message string) string {
	foo := fmt.Sprint(message)
	return foo
}

func main() {
	Hello("hello")
}
