package main

import (
	"testing"
)

func TestHello(t *testing.T) {
	if v := Hello("hello"); v != "hello" {
		t.Errorf("Expected 'hello' but got %s ", v)
	}
}
